import 'package:flutter/material.dart';
import 'package:payflow/shared/themes/appcolors.dart';

class DividerVerticalWidget extends StatelessWidget {
  const DividerVerticalWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.stroke,
      height: double.maxFinite,
      width: 1,
      child: null,
    );
  }
}
