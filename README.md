# PayFlow

Projeto criado a partir da [NLW6](https://nextlevelweek.com/inscricao/6) de 20 a 27 de Junho.

O treinamento segue com o foco em animações feitas utilizando o framework [Flutter](https://flutter.dev/docs).


## Design

Design fornecido durante as aulas.

Documentação do design completa no [Figma](https://www.figma.com/file/kLK7FYnWKMoN68sQXcSniu/duplicate).


## [Notion](https://www.notion.so/Mission-Flutter-9d2a1e0818b64b61bc5d9a0424f5c766)

O projeto foi feito seguinndo o seguinte cronograma:

* [Configuração do ambiente](https://www.notion.so/Configura-es-do-ambiente-84ee5ebc045745ad996e4f18701f42bf)

* [[20/06] Aula 01 - Primeiros passos com Dart e Flutter](https://www.notion.so/20-06-Aula-01-Primeiros-passos-com-Dart-e-Flutter-d88723c8c2114096ba44f6b0a369910a)

* [[21/06] Aula 02 - Mão na massa](https://www.notion.so/21-06-Aula-02-M-o-na-massa-96d18f2a435244a886fa6687c1d3847f)

* [[22/06] Aula 03 - Aprendendo sobre o Firebase](https://www.notion.so/22-06-Aula-03-Aprendendo-sobre-o-Firebase-f584fabf86094d89b39beaaf13870b00)

* [[23/06] Aula 04 - Aprendendo sobre o MLKIT](https://www.notion.so/23-06-Aula-04-Aprendendo-sobre-o-MLKIT-5456caf404874563816f0f7b58f61ac8)

* [[24/06] Aula 05 - Finalizando o App e animando!](https://www.notion.so/24-06-Aula-05-Finalizando-o-App-e-animando-621062a3880241608de1f628076d457a)

## Cronograma das etapas

1. [20/06] Liftoff
2. [21/06] Maximum Speed
3. [22/06] In Orbit
4. [23/06] Landing
5. [24/06] Surface Exploration